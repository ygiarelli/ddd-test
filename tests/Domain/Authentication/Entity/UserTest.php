<?php

declare(strict_types=1);

namespace App\Tests\Domain\Authentication\Entity;

use App\Domain\Authentication\Entity\User;
use App\Domain\Authentication\Value\PlainPassword;
use App\Domain\Value\EmailAddress;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function test_registered_user_can_be_authenticated()
    {
        $user = User::register(EmailAddress::fromString('foo@bar.baz'), PlainPassword::fromString('foobarbaz'));

        $this->assertInstanceOf(User::class, $user);
        $this->assertTrue($user->checkCredentials(PlainPassword::fromString('foobarbaz')));
    }
}
