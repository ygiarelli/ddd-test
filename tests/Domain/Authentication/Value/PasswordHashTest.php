<?php

declare(strict_types=1);

namespace App\Tests\Domain\Authentication\Value;

use App\Domain\Authentication\Value\PasswordHash;
use PHPUnit\Framework\TestCase;

class PasswordHashTest extends TestCase
{
    public function test_it_builds_from_string()
    {
        $this->assertInstanceOf(
            PasswordHash::class,
            PasswordHash::fromString(password_hash('foobarbaz', \PASSWORD_ARGON2I))
        );
    }
}
