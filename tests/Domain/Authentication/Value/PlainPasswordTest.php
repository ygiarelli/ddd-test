<?php

declare(strict_types=1);

namespace App\Tests\Domain\Authentication\Value;

use App\Domain\Authentication\Value\PasswordHash;
use App\Domain\Authentication\Value\PlainPassword;
use PHPUnit\Framework\TestCase;

class PlainPasswordTest extends TestCase
{
    public function test_it_accept_valid_password()
    {
        $this->assertInstanceOf(PlainPassword::class, PlainPassword::fromString('This is a passphrase'));
    }

    /**
     * @expectedException \App\Domain\Exception\ValidationException
     * @expectedExceptionMessage Password too short. Minimum 8 characters
     */
    public function test_it_denies_invalid_password()
    {
        PlainPassword::fromString('foobar');
    }

    public function test_it_is_hashable()
    {
        $this->assertInstanceOf(PasswordHash::class, PlainPassword::fromString('foobarbaz')->toHash());
    }

    public function test_it_verifies_against_own_hash()
    {
        $plainPassword = PlainPassword::fromString('foobarbaz');
        $this->assertTrue($plainPassword->verifyAgainstHash($plainPassword->toHash()));
    }
}
