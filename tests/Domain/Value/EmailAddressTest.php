<?php

declare(strict_types=1);

namespace App\Tests\Domain\Value;

use App\Domain\Value\EmailAddress;
use PHPUnit\Framework\TestCase;

class EmailAddressTest extends TestCase
{
    public function test_it_accepts_valid_email_address()
    {
        $this->assertInstanceOf(EmailAddress::class, EmailAddress::fromString('foo@bar.baz'));
    }

    /**
     * @expectedException \App\Domain\Exception\ValidationException
     * @expectedExceptionMessage "foo@barbaz" is not a valid email address
     */
    public function test_it_denies_invalid_email_address()
    {
        EmailAddress::fromString('foo@barbaz');
    }
}
