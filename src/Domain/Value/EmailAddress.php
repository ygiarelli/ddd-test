<?php

declare(strict_types=1);

namespace App\Domain\Value;

use App\Domain\Exception\ValidationException;

final class EmailAddress
{
    private $address;

    private function __construct(string $address)
    {
        $this->address = $address;
    }

    public function equals(self $emailAddress): bool
    {
        return $emailAddress->address === $this->address;
    }

    public static function fromString(string $address): self
    {
        if (!filter_var($address, \FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException(sprintf('"%s" is not a valid email address', $address));
        }

        return new self($address);
    }

    public function __toString(): string
    {
        return $this->address;
    }
}
