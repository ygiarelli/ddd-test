<?php

declare(strict_types=1);

namespace App\Domain\Authentication\Query;

interface ListUsersQuery
{
    public function __invoke(): array;
}
