<?php

declare(strict_types=1);

namespace App\Domain\Authentication\Entity;

use App\Domain\Authentication\Value\PasswordHash;
use App\Domain\Authentication\Value\PlainPassword;
use App\Domain\Value\EmailAddress;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="email", nullable=false)
     *
     * @var EmailAddress
     */
    private $email;

    /**
     * @ORM\Column(type="password_hash", nullable=false)
     *
     * @var PasswordHash
     */
    private $password;

    public function checkCredentials(PlainPassword $plainPassword): bool
    {
        return $plainPassword->verifyAgainstHash($this->password);
    }

    public static function register(EmailAddress $emailAddress, PlainPassword $plainPassword): self
    {
        $user           = new self;
        $user->email    = $emailAddress;
        $user->password = $plainPassword->toHash();

        return $user;
    }
}
