<?php

declare(strict_types=1);

namespace App\Domain\Authentication\Repository;

use App\Domain\Authentication\Entity\User;

interface Users
{
    public function store(User $user): void;

    /**
     * @return User[]
     */
    public function findAll(): array;
}
