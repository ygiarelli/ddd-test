<?php

declare(strict_types=1);

namespace App\Domain\Authentication\Value;

final class PasswordHash
{
    /**
     * @var string
     */
    private $hash;

    private function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    public static function fromString(string $hash): self
    {
        return new self($hash);
    }

    public function __toString(): string
    {
        return $this->hash;
    }
}
