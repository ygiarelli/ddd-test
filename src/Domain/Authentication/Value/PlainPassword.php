<?php

declare(strict_types=1);

namespace App\Domain\Authentication\Value;

use App\Domain\Exception\ValidationException;

final class PlainPassword
{
    const MIN_LENGTH = 8;

    /**
     * @var string
     */
    private $password;

    private function __construct(string $password)
    {
        $this->password = $password;
    }

    public static function fromString(string $password): self
    {
        if (mb_strlen($password) < self::MIN_LENGTH) {
            throw new ValidationException(sprintf('Password too short. Minimum %s characters.', self::MIN_LENGTH));
        }

        return new self($password);
    }

    public function toHash(): PasswordHash
    {
        return PasswordHash::fromString(password_hash($this->password, \PASSWORD_ARGON2I));
    }

    public function verifyAgainstHash(PasswordHash $passwordHash): bool
    {
        return password_verify($this->password, (string)$passwordHash);
    }

    public function __toString(): string
    {
        return 'Plain password (hidden)';
    }
}
