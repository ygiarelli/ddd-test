<?php

declare(strict_types=1);

namespace App\Domain\Exception;

class ValidationException extends \InvalidArgumentException implements Exception
{
    /**
     * @var ?string
     */
    private $path;

    /**
     * @var ?object
     */
    private $object;

    public function __construct(string $message, ?object $object = null, string $path = null)
    {
        $this->path   = $path;
        $this->object = $object;

        parent::__construct($message);
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getObject(): ?object
    {
        return $this->object;
    }
}
