<?php

declare(strict_types=1);

namespace App\Application\Authentication\ViewModel;

use App\Application\ViewModel;

final class ListUsersViewModel implements ViewModel
{
    /**
     * @var array
     */
    private $users;

    public function __construct(array $users)
    {
        $this->users = $users;
    }

    public function getUsers(): array
    {
        return $this->users;
    }

    public function getTemplate(): string
    {
        return 'authentication/users/list.html.twig';
    }
}
