<?php

declare(strict_types=1);

namespace App\Application\Authentication\Controller\User;

use App\Application\Authentication\ViewModel\ListUsersViewModel;
use App\Application\Controller;
use App\Domain\Authentication\Query\ListUsersQuery;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/users", name="authentication_users", methods={"GET"})
 */
final class ListController implements Controller
{
    /**
     * @var ListUsersQuery
     */
    private $listUsersQuery;

    public function __construct(ListUsersQuery $listUsersQuery)
    {
        $this->listUsersQuery = $listUsersQuery;
    }

    public function __invoke()
    {
        return new ListUsersViewModel(call_user_func($this->listUsersQuery));
    }
}
