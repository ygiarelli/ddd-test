<?php

declare(strict_types=1);

namespace App\Application\Dashboard\ViewModel;

use App\Application\ViewModel;

final class DashboardViewModel implements ViewModel
{
    public function getTemplate(): string
    {
        return 'dashboard/dashboard.html.twig';
    }

    public function getTemplateContext(): array
    {
        return [];
    }
}
