<?php

declare(strict_types=1);

namespace App\Application\Dashboard\Controller;

use App\Application\Dashboard\ViewModel\DashboardViewModel;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="dashboard_dashboard", methods={"GET"})
 */
final class DashboardController implements Controller
{
    public function __invoke(): DashboardViewModel
    {
        return new DashboardViewModel;
    }
}
