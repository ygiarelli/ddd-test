<?php

declare(strict_types=1);

namespace App\Application;

interface ViewModel
{
    public function getTemplate(): string;
}
