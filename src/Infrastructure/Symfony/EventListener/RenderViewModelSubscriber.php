<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\EventListener;

use App\Application\ViewModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

final class RenderViewModelSubscriber implements EventSubscriberInterface
{
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if (!($viewModel = $event->getControllerResult()) instanceof ViewModel) {
            return;
        }

        $event->setResponse(
            new Response($this->twig->render($viewModel->getTemplate(), ['view_model' => $viewModel]))
        );
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => 'onKernelView',
        ];
    }
}
