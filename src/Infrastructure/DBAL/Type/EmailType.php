<?php

declare(strict_types=1);

namespace App\Infrastructure\DBAL\Type;

use App\Domain\Value\EmailAddress;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class EmailType extends StringType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (string)$value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return EmailAddress::fromString($value);
    }

    public function getName()
    {
        return 'email';
    }
}
