<?php

declare(strict_types=1);

namespace App\Infrastructure\DBAL\Type;

use App\Domain\Authentication\Value\PasswordHash;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class PasswordHashType extends StringType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (string)$value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return PasswordHash::fromString($value);
    }

    public function getName()
    {
        return 'password_hash';
    }
}
