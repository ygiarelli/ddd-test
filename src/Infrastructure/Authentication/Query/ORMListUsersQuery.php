<?php

declare(strict_types=1);

namespace App\Infrastructure\Authentication\Query;

use App\Domain\Authentication\Entity\User;
use App\Domain\Authentication\Query\ListUsersQuery;
use Doctrine\ORM\EntityManagerInterface;

class ORMListUsersQuery implements ListUsersQuery
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function __invoke(): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('user')
            ->from(User::class, 'user')
            ->orderBy('user.email')
            ->getQuery()
            ->getResult();
    }
}
